'use strict';

const express = require('express');
const cors = require('cors');
const Keycloak = require('keycloak-connect');
const mongoRouter = require('./routes/mongo');
const authRouter = require('./routes/auth');
const dataRouter = require('./routes/data');

const app = express();
const port = 3300;

app.use(cors());

const keycloak = new Keycloak({});

app.use('/mongo', mongoRouter);
app.use('/auth', authRouter);


app.use(keycloak.middleware());

app.use('/data', dataRouter);


// tenant-admin is a client level role
app.get('/data', keycloak.enforcer('Data',{response_mode: 'permissions'}), (req, res, next) => {
  const data = [];
  const rand = Math.floor(Math.random() * 10);

  for(let i = 0; i < rand; i++) {
    data.push({
      message: `data record #${i}`
    });
  }

  res.json(data);
});

// app.post('/data', keycloak.enforcer('Data',{response_mode: 'permissions'}), (req, res, next) => {
//   res.json(req.body);
// });

app.listen(port, () => console.log(`app listening on port ${port}!`))
