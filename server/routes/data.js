var express = require('express');
var router = express.Router();
const Keycloak = require('keycloak-connect');

let dataController = require('../controllers/data');
const keycloak = new Keycloak({});

router.get('/', keycloak.enforcer(), dataController.get)

module.exports = router;