var express = require('express');
var router = express.Router();

let mongoController = require('../controllers/mongoDBConfig');

router.get('/enable', mongoController.enableMongo);

router.get('/createRole', mongoController.createRole);

router.get('/config', mongoController.mongoConfig);

router.get('/createToken', mongoController.createToken);

router.get('/getCred', mongoController.mongoCred);

module.exports = router;