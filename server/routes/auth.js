var express = require('express');
var router = express.Router();

let authController = require('../controllers/auth');

router.get('/enableKv', authController.enablekv);

router.get('/enableJwt', authController.enablejwt);

router.get('/createPolicy', authController.createPolicy);

router.get('/createRole', authController.createRole);

router.get('/config', authController.jwtConfig);

router.get('/login', authController.login);

router.get('/writeSecrete',authController.writeSecrets)

module.exports = router;