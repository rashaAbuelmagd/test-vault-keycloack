const request = require('request-promise');
const rolename = 'readwrite';
const policyName = 'admin:readwrite'
const applicationName = 'trueconnect';

//enable Mongo
exports.enableMongo = async (req,res) => {
    const options = {
        method: `POST`
        , json: true
        , uri: `http://127.0.0.1:8200/v1/sys/mounts/database/${applicationName}`
        , body: { "type": "database" }
        , headers: {
            'X-Vault-Token': 's.GEseO1mZx3rxtIM07XvT3QqB'
        }
    };
    try {
        const response = await request(options);
        return res.json(response);
    }
    catch (error) {        
        return res.send(error.message)
    }
} 

//create role

exports.createRole = async (req,res) => {
    const options = {
        method: `POST`
        , json: true
        , uri: `http://127.0.0.1:8200/v1/database/${applicationName}/roles/${rolename}`
        , body: {
            "db_name": "mongodb",
            "creation_statements": ["{\"db\":\"archDB\", \"roles\": [{\"role\": \"readWrite\",\"db\": \"archDB\"}]}"],
            "revocation_statements": ["{\"db\":\"archDB\", \"roles\": [{\"role\": \"readWrite\",\"db\": \"archDB\"}]}"],
            "default_ttl": "0.25h",
            "max_ttl": "0.25h"
        }
        , headers: {
            'X-Vault-Token': 's.GEseO1mZx3rxtIM07XvT3QqB'
        }
    };
    try {
        const response = await request(options);
        return res.json(response);
    }
    catch (error) {
        res.send(error.message)
    }
}

//create mongo config

exports.mongoConfig = async (req,res) => {
    const options = {
        method: `POST`
        , json: true
        , uri: `http://127.0.0.1:8200/v1/database/${applicationName}/config/mongodb`
        , body: {
            "plugin_name": "mongodb-database-plugin",
            "allowed_roles": `${rolename}`,
            "connection_url": "mongodb://localhost:27017/",
            "write_concern": "",
            "username": "",
            "password": ""
        }
        , headers: {
            'X-Vault-Token': 's.GEseO1mZx3rxtIM07XvT3QqB'
        }
    };
    try {
        const response = await request(options);
        return res.json(response);
    }
    catch (error) {
        res.send(error.message)
    }
}

exports.createToken = async (req,res) => {
    const options = {
        method: `POST`
        , json: true
        , uri: `http://127.0.0.1:8200/v1/auth/token/create`
        , body: {
            "policies": [
                `${policyName}`
            ]
        }
        , headers: {
            'X-Vault-Token': 's.GEseO1mZx3rxtIM07XvT3QqB'
        }
    };
    try {
        const response = await request(options);
        return res.json(response);
    }
    catch (error) {
        res.send(error.message)
    }
}

//get mongo credentials
//add vault token
exports.mongoCred = async(req,res) => {
    const options = {
        method: `GET`
        , json: true
        , uri: `http://127.0.0.1:8200/v1/database/creds/${rolename}`
        , headers: {
            'X-Vault-Token': 's.GEseO1mZx3rxtIM07XvT3QqB'
        }
    };
    try {
        const response = await request(options);
        return res.json(response);
    }
    catch (error) {
        res.send(error.message)
    }
}