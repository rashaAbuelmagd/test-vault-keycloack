const request = require('request-promise');
const rolename = 'truflow-admin';
const policyName = 'admin:readwrite';
const serviceName = 'trueflow';
const realmName = 'sharp_insurance';

//enable jwt
exports.enablejwt = async (req, res) => {
    const options = {
        method: `POST`
        , json: true
        , uri: `http://127.0.0.1:8200/v1/sys/auth/${serviceName}/jwt`
        , body: { "type": "jwt" }
        , headers: {
            'X-Vault-Token': 's.GEseO1mZx3rxtIM07XvT3QqB'
        }
    };
    try {
        const response = await request(options);
        return res.json(response);
    }
    catch (error) {
        return res.send(error.message)
    }
}

//enable kv secret engine for the trueflow service
exports.enablekv = async (req, res) => {
    const options = {
        method: `POST`
        , json: true
        , uri: `http://127.0.0.1:8200/v1/sys/mounts/${realmName}/${serviceName}/secrets`
        , body: { "type": "kv" }
        , headers: {
            'X-Vault-Token': 's.GEseO1mZx3rxtIM07XvT3QqB'
        }
    };
    try {
        const response = await request(options);
        return res.json(response);
    }
    catch (error) {
        return res.send(error.message)
    }
}

//create acl Policy 
exports.createPolicy = async (req, res) => {
    const options = {
        method: `POST`
        , json: true
        , uri: `http://127.0.0.1:8200/v1/sys/policies/acl/${policyName}`
        , body: {
            "policy": "path \"/{{identity.entity.metadata.realm}}/{{identity.entity.aliases.auth_jwt_fbe1d3d1.metadata.service}}/secrets/*\" {capabilities = [\"create\", \"read\", \"list\"]}"
        }
        , headers: {
            'X-Vault-Token': 's.GEseO1mZx3rxtIM07XvT3QqB'
        }
    };
    try {
        const response = await request(options);
        return res.json(response);
    }
    catch (error) {
        res.send(error.message)
    }
}

//create role
exports.createRole = async (req, res) => {
    const options = {
        method: `POST`
        , json: true
        , uri: `http://127.0.0.1:8200/v1/auth/${serviceName}/jwt/role/${rolename}`
        , body: {
            "policies": [
                `${policyName}`
            ],
            "bound_issuer": "iss",
            "claim_mappings": {
                "preferred_username": "preferred_username",
                "service" : "service"
            },
            "bound_audiences": "account",
            "user_claim": "sub",
            "role_type": "jwt"
        }
        , headers: {
            'X-Vault-Token': 's.GEseO1mZx3rxtIM07XvT3QqB'
        }
    };
    try {
        const response = await request(options);
        return res.json(response);
    }
    catch (error) {
        res.send(error.message)
    }
}

//create jwt config
exports.jwtConfig = async (req, res) => {
    const options = {
        method: `POST`
        , json: true
        , uri: `http://127.0.0.1:8200/v1/auth/${serviceName}/jwt/config`
        , body: { "jwks_url": "http://localhost:8080/auth/realms/sharp_insurance/protocol/openid-connect/certs" }
        , headers: {
            'X-Vault-Token': 's.GEseO1mZx3rxtIM07XvT3QqB'
        }
    };
    try {
        const response = await request(options);
        return res.json(response);
    }
    catch (error) {
        res.send(error.message)
    }
}

// login with the role and jwt to get client token for saving secrets
exports.login = async (req, res) => {
    const jwt = req.headers.authorization

    const options = {
        method: `POST`
        , json: true
        , uri: `http://127.0.0.1:8200/v1/auth/${serviceName}/jwt/login`
        , body: {
            "role": `${rolename}`,
            "jwt": `${jwt}`
        }
        , headers: {
            'X-Vault-Token': 's.GEseO1mZx3rxtIM07XvT3QqB'
        }
    };
    try {
        const response = await request(options);
        return res.json(response);
    }
    catch (error) {
        res.send(error.message)
    }
}

//use the generated client token from the login endpoint in the header as X-Vault-Token
exports.writeSecrets = async (req, res) => {
    const client_token = req.headers.X-Vault-Token;
    const options = {
        method: `POST`
        , json: true
        , uri: `http://127.0.0.1:8200/v1/${realmName}/${serviceName}/secrets/TestCred`
        , body: {
            "data": {
                "credentials": "Hellooooooooooooo"
            }
        }
        , headers: {
            'X-Vault-Token': client_token
        }
    };
    try {
        const response = await request(options);
        return res.json(response);
    }
    catch (error) {
        return res.send(error.message)
    }
}

//Create Entity
exports.createEntity = async (req, res) => {
    const options = {
        method: `POST`
        , json: true
        , uri: `http://127.0.0.1:8200/v1/identity/entity`
        , body: {
            "metadata": {
                  "realm": realmName,
                  "service":serviceName
              },
            "policies": ["admin:readwrite"]
          }
        , headers: {
            'X-Vault-Token': "s.GEseO1mZx3rxtIM07XvT3QqB"
        }
    };
    try {
        const response = await request(options);
        return res.json(response);
    }
    catch (error) {
        return res.send(error.message)
    }
}

//Create Entity Alias
exports.createEntity = async (req, res) => {
    const options = {
        method: `POST`
        , json: true
        , uri: `http://127.0.0.1:8200/v1/identity/entity-alias`
        , body: {
            "name": "truflowJWT",
            "canonical_id": "794d5322-692c-d04a-696c-0968007aa0e3", //Entity ID
            "mount_accessor": "auth_jwt_fbe1d3d1" //jwt secret engine accessor id gets from http://127.0.0.1:8200/v1/sys/auth GET
          }
        , headers: {
            'X-Vault-Token': "s.GEseO1mZx3rxtIM07XvT3QqB"
        }
    };
    try {
        const response = await request(options);
        return res.json(response);
    }
    catch (error) {
        return res.send(error.message)
    }
}