import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  private readonly httpClient: HttpClient;
  title = 'client';
  items: any[] = [];

  constructor(httpClient: HttpClient) {
    this.httpClient = httpClient;
  }

  async getData() {
    const self: any = this;

    this.httpClient.get('http://localhost:3300/data').subscribe(data => {
      self.items = data;
    });
  }

  async addData() {
    const self: any = this;

    this.httpClient.post('http://localhost:3300/data',{"Name":"Test","Age":"25","Country":"Egypt","Phone":"123456"}).subscribe(data => {
      self.items = data;
    });
  }
}
