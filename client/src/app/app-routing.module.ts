import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanAuthenticationGuard } from './auth.guard';
import { AppModule } from './app.module';
import { AppComponent } from './app.component';


const routes: Routes = [
  {
    path: '*',
    component: AppComponent,
    canActivate: [CanAuthenticationGuard],
    data: { roles: ['User'] }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  providers: [CanAuthenticationGuard],
  exports: [RouterModule]
})
export class AppRoutingModule { }
